require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

process.env.CONFIG_FILE = "_mocha-config-dev.json"
var config = require('MonsterConfig');


var walk = require('walk')
var path = require("path")

if(!process.argv[3]){
   console.log("Usage: node %s locale_dir dir_with_html_files", process.argv[1]);
   process.reallyExit();
}
var i = config.get("i18n-2")
i.directory = process.argv[2]
config.set("i18n-2", i)
console.log(config.get("i18n-2"));
//process.reallyExit()

var templates = require("MonsterTemplates")(config.get("i18n-2"))

var walker = walk.walk(process.argv[3]);

var fs = require("fs")

walker.on("file", function (root, fileStats, next) {
  if(fileStats.name.match(/\.html?$/)){
	  var fp = path.join(root, fileStats.name)
	  for(var locale of config.get("i18n-2").locales) {
		 console.log(fp, ":", locale)
		 fs.readFile(fp, function(err,data){
		 	data = new String(data)
		 	data = data.replace(/{% *extends *'[^']*' *%}/g, ""); 
			 templates.renderAsync(data, {}, {subject: true, locale: locale, stripAngularParams: true})
			   .catch(x=>{
				  console.log(x)
			   })
		 
		 })
	  }
  }
  next()
});
