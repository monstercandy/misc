{
	"Have a nice day!": "Have a nice day!",
	"Email confirmation": "Email confirmation",
	"Dear %s,": "Dear %s,",
	"This email is about verification of your email address.": "This email is about verification of your email address.",
	"If it was not you requesting this action, please contact our customer service immediately:": "If it was not you requesting this action, please contact our customer service immediately:",
	"To confirm validity of your email address, please open the following link:": "To confirm validity of your email address, please open the following link:",
	"Forgotten password": "Forgotten password",
	"Did you forget the password to your account? This email helps you recovering the access to it.": "Did you forget the password to your account? This email helps you recovering the access to it.",
	"If you open the following link, you can enter our administration interface and will be able to change your password to a new one.": "If you open the following link, you can enter our administration interface and will be able to change your password to a new one.",
	"Please note that the link below is valid for %d hours only, then access will be denied.": "Please note that the link below is valid for %d hours only, then access will be denied.",
	"Primary email has changed": "Primary email has changed",
	"This is a notification email, the primary email address of your account has changed.": "This is a notification email, the primary email address of your account has changed.",
	"Old email address": "Old email address",
	"New email address": "New email address",
	"This also means you need to use your new email address when logging in to the administration interface.": "This also means you need to use your new email address when logging in to the administration interface.",
	"Registration email": "Registration email",
	"Thank you for trusting us and choosing our service to host your web application!": "Thank you for trusting us and choosing our service to host your web application!",
	"We created an account in the Monster Media system for you with the following details:": "We created an account in the Monster Media system for you with the following details:",
	"Name": "Name",
	"Email": "Email",
	"Password": "Password",
	"not disclosed due to security reasons": "not disclosed due to security reasons",
	"You can login to our administration interface via the following link": "You can login to our administration interface via the following link"
}